using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;



public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startlives;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private TextMeshProUGUI finalCoinsCount;
    [SerializeField] private string firstLevelName = "level 1";

    private int lives;
    private int coins;


    private void Awake()
    {
        if(game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        lives = startlives;
        coins = 0;
        ShowLives();
        ShowCoins();
    }
    
    public void LoseLive()
    {
        lives--;
        if (lives <= 0)
        {
            lives = 0;
            GameOver();
        }
        ShowLives();
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        mainMenu.SetActive(true);
        finalCoinsCount.text = coins.ToString();
    }
    public void RestartGame()
    {
        lives = startlives;
        coins = 0;
        ShowLives();
        ShowCoins();
        SceneManager.LoadScene(firstLevelName);
        mainMenu.SetActive(false);
        Time.timeScale = 1f;
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void AddCoins(int amount)
    {
        coins += amount;
        ShowCoins();
    }

    public void ShowLives()
    {
        livesText.text = lives.ToString();
    }

    public void ShowCoins()
    {
        coinsText.text = coins.ToString();
    }
}
